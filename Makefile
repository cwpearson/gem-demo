all: deviceQuery.cu
	nvcc -O3 -ccbin=g++ -Xcompiler -Wall,-Wextra,-Wshadow -o deviceQuery $^

clean:
	rm -f deviceQuery
